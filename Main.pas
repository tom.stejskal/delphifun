unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, MaybeFun, ListFun, EitherFun,
  System.Generics.Collections;

type
  TMainForm = class(TForm)
    Memo: TMemo;
    Run: TButton;
    procedure RunClick(Sender: TObject);
  private
    procedure PrintLine(const ALine: String);
    procedure PrintList(const AList: IList<String>);
    procedure PrintDiv;
    procedure TestListMap;
    procedure TestListMapIdiom;
    procedure TestListFilter;
    procedure TestListFilterIdiom;
    procedure TestListReverse;
    procedure TestListSum;
    procedure TestListSumIdiom;
    procedure TestListProduct;
    procedure TestListProductIdiom;
    procedure TestListAll;
    procedure TestListAny;
    procedure TestMaybeMap;
    procedure TestMaybeAndThen;
    procedure TestEitherMap;
    procedure TestEitherAndThen;
  end;

var
  MainForm: TMainForm;

implementation

function StrToIntMaybe(const S: String): IMaybe<Integer>;
var
  mValue: Integer;
begin
  if TryStrToInt(S, mValue) then begin
    Result := Maybe.Just<Integer>(mValue);
  end else begin
    Result := Maybe.Nothing<Integer>;
  end;
end;

function StrToIntEither(const S: String): IEither<String, Integer>;
var
  mValue: Integer;
begin
  if TryStrToInt(S, mValue) then begin
    Result := Either.Right<String, Integer>(mValue);
  end else begin
    Result := Either.Left<String, Integer>(Format('Cannot convert "%0:s" to integer', [S]));
  end;
end;

{$R *.dfm}

procedure TMainForm.PrintDiv;
begin
  PrintLine('--------------------------------------------------------------------------------');
end;

procedure TMainForm.PrintLine(const ALine: String);
begin
  Memo.Lines.Add(ALine);
end;

procedure TMainForm.PrintList(const AList: IList<String>);
begin
  List.Each<String>(procedure(X: String) begin PrintLine(X) end, AList);
end;

procedure TMainForm.RunClick(Sender: TObject);
begin
  Memo.Clear;
  TestListMap;
  TestListMapIdiom;
  TestListFilter;
  TestListFilterIdiom;
  TestListReverse;
  TestListSum;
  TestListSumIdiom;
  TestListProduct;
  TestListProductIdiom;
  TestListAll;
  TestListAny;
  TestMaybeMap;
  TestMaybeAndThen;
  TestEitherMap;
  TestEitherAndThen;
end;

procedure TMainForm.TestEitherAndThen;
begin
  PrintLine('TestEitherAndThen');
  Either.Match<String, String, String>(
    function(AError: String): String begin
      PrintLine(Format('Error: %0:s', [AError]));
      Result := '';
    end,
    function(X: String): String begin
      PrintLine(X);
      Result := X;
    end,
    Either.Map<String, Integer, String>(IntToStr,
      Either.AndThen<String, Integer, Integer>(
        function(X: Integer): IEither<String, Integer> begin
          Result := Either.AndThen<String, Integer, Integer>(
            function(Y: Integer): IEither<String, Integer> begin
              Result := Either.Map<String, Integer, Integer>(
                function(Z: Integer): Integer begin
                  Result := X + Y + Z;
                end,
                StrToIntEither('56'));
            end,
            StrToIntEither('3'));
        end,
        StrToIntEither('27'))));
  PrintDiv;
end;

procedure TMainForm.TestEitherMap;
begin
  PrintLine('TestEitherMap');
  Either.Match<String, String, String>(
    function(AError: String): String begin
      PrintLine(Format('Error: %0:s', [AError]));
      Result := '';
    end,
    function(X: String): String begin
      PrintLine(X);
      Result := X;
    end,
    Either.Map<String, Integer, String>(IntToStr,
      Either.Map<String, Integer, Integer>(function(X: Integer): Integer begin Result := X * 2 end,
        StrToIntEither('27'))));
  PrintDiv;
end;

procedure TMainForm.TestListAll;
begin
  PrintLine('TestListAll');
  PrintLine(BoolToStr(
    List.All<Integer>(function(X: Integer): Boolean begin Result := X > 1 end,
      List.FromArray<Integer>([1, 2, 3, 4, 5])), True));
  PrintDiv;
end;

procedure TMainForm.TestListAny;
begin
  PrintLine('TestListAny');
  PrintLine(BoolToStr(
    List.Any<Integer>(function(X: Integer): Boolean begin Result := X > 1 end,
      List.FromArray<Integer>([1, 2, 3, 4, 5])), True));
  PrintDiv;
end;

procedure TMainForm.TestListFilter;
begin
  PrintLine('TestListFilter');
  PrintLine(
    FloatToStr(
      List.Product(
        List.Map<Integer, Extended>(function(X: Integer): Extended begin Result := X end,
          List.Filter<Integer>(function(X: Integer): Boolean begin Result := X mod 2 = 0 end,
            List.Map<Integer, Integer>(function (X: Integer): Integer begin Result := X * 3 end,
              List.FromArray<Integer>([1, 2, 3, 4, 5])))))));
  PrintDiv;
end;

procedure TMainForm.TestListFilterIdiom;
var
  mList: TList<Integer>;
  mProduct: Extended;
  I: Integer;
begin
  PrintLine('TestListFilterIdiom');
  mProduct := 1;
  mList := TList<Integer>.Create;
  try
    mList.AddRange([1, 2, 3, 4, 5]);
    for I := 0 to mList.Count - 1 do begin
      mList[I] := mList[I] * 3;
      if mList[I] mod 2 = 0 then begin
        mProduct := mProduct * mList[I];
      end;
    end;
  finally
    mList.Free;
  end;
  PrintLine(FloatToStr(mProduct));
  PrintDiv;
end;

procedure TMainForm.TestListMap;
begin
  PrintLine('TestListMap');
  PrintList(
    List.Map<Integer, String>(IntToStr,
      List.Map<Integer, Integer>(function(X: Integer): Integer begin Result := X * 3 end,
        List.FromArray<Integer>([1, 2, 3]))));
  PrintDiv;
end;

procedure TMainForm.TestListMapIdiom;
var
  mList: TList<Integer>;
  mProduct: Extended;
  I: Integer;
begin
  PrintLine('TestListMapIdiom');
  mProduct := 1;
  mList := TList<Integer>.Create;
  try
    mList.AddRange([1, 2, 3]);
    for I := 0 to mList.Count - 1 do begin
      PrintLine(FloatToStr(mList[I] * 3));
    end;
  finally
    mList.Free;
  end;
  PrintDiv;
end;

procedure TMainForm.TestListProduct;
begin
  PrintLine('TestListProduct');
  PrintLine(FloatToStr(
    List.Product(
      List.FromArray<Extended>([1, 2, 3, 4, 5]))));
  PrintDiv;
end;

procedure TMainForm.TestListProductIdiom;
var
  mList: TList<Integer>;
  mProduct: Extended;
  I: Integer;
begin
  PrintLine('TestListProductIdiom');
  mProduct := 1;
  mList := TList<Integer>.Create;
  try
    mList.AddRange([1, 2, 3, 4, 5]);
    for I := 0 to mList.Count - 1 do begin
      mProduct := mProduct * mList[I];
    end;
  finally
    mList.Free;
  end;
  PrintLine(FloatToStr(mProduct));
  PrintDiv;
end;

procedure TMainForm.TestListReverse;
begin
  PrintLine('TestListReverse');
  PrintList(
    List.Map<Integer, String>(IntToStr,
      List.Reverse<Integer>(
        List.FromArray<Integer>([1, 2, 3]))));
  PrintDiv;
end;

procedure TMainForm.TestListSum;
begin
  PrintLine('TestListSum');
  PrintLine(FloatToStr(
    List.Sum(
      List.FromArray<Extended>([1, 2, 3, 4, 5]))));
  PrintDiv;
end;

procedure TMainForm.TestListSumIdiom;
var
  mList: TList<Integer>;
  mSum: Extended;
  I: Integer;
begin
  PrintLine('TestListSumIdiom');
  mSum := 0;
  mList := TList<Integer>.Create;
  try
    mList.AddRange([1, 2, 3, 4, 5]);
    for I := 0 to mList.Count - 1 do begin
      mSum := mSum + mList[I];
    end;
  finally
    mList.Free;
  end;
  PrintLine(FloatToStr(mSum));
  PrintDiv;
end;

procedure TMainForm.TestMaybeAndThen;
begin
  PrintLine('TestMaybeAndThen');
  Maybe.Match<String, String>(
    function: String begin
      PrintLine('Error: conversion from string to integer failed');
      Result := '';
    end,
    function(X: String): String begin
      PrintLine(X);
      Result := X;
    end,
    Maybe.Map<Integer, String>(IntToStr,
      Maybe.AndThen<Integer, Integer>(
        function(X: Integer): IMaybe<Integer> begin
          Result := Maybe.AndThen<Integer, Integer>(
            function(Y: Integer): IMaybe<Integer> begin
              Result := Maybe.Map<Integer, Integer>(
                function(Z: Integer): Integer begin
                  Result := X + Y + Z;
                end,
                StrToIntMaybe('56'));
            end,
            StrToIntMaybe('3'));
        end,
        StrToIntMaybe('27'))));
  PrintDiv;
end;

procedure TMainForm.TestMaybeMap;
begin
  PrintLine('TestMaybeMap');
  Maybe.Match<String, String>(
    function: String begin
      PrintLine('Nothing');
      Result := '';
    end,
    function(X: String): String begin
      PrintLine(X);
      Result := X;
    end,
    Maybe.Map<Integer, String>(IntToStr,
      Maybe.Map<Integer, Integer>(function(X: Integer): Integer begin Result := X * 2 end,
        StrToIntMaybe('27'))));
  PrintDiv;
end;

end.
