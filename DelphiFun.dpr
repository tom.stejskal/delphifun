program DelphiFun;

uses
  Vcl.Forms,
  Main in 'Main.pas' {MainForm},
  MaybeFun in 'MaybeFun.pas',
  ListFun in 'ListFun.pas',
  EitherFun in 'EitherFun.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
